-- iplantv1.r_config definition

CREATE TABLE `r_config` (
  `code` varchar(50) NOT NULL,
  `type` varchar(10) DEFAULT NULL,
  `value` text,
  `description` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- iplantv1.m_medicine definition

CREATE TABLE `m_medicine` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `disease` varchar(200) DEFAULT NULL,
  `cure` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- iplantv1.t_histories definition

CREATE TABLE `t_histories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `disease` varchar(200) DEFAULT NULL,
  `cure` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
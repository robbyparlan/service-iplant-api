import * as express from 'express';

//Controller
import PlantController from '../../controllers/PlantController';
const PlantCtl = new PlantController()

let router = express.Router()

router.post('/submit', PlantCtl.captureImage)

export default router
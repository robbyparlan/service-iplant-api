import 'dotenv/config';

const yaml = require("js-yaml");
const fs = require("fs");

let yamlFile = fs.readFileSync("./app.yaml", "utf8");
let loadedYaml = yaml.load(yamlFile);

let configDB = {
  client: loadedYaml.env_variables.DB_CONNECTION,
  connection: {
    host : loadedYaml.env_variables.DB_HOST,
    port : loadedYaml.env_variables.DB_PORT,
    user : loadedYaml.env_variables.DB_USER,
    password : loadedYaml.env_variables.DB_PASSWORD,
    database : loadedYaml.env_variables.DB_DATABASE,
    // socketPath : loadedYaml.env_variables.DB_HOST,
  },
  pool: {
    min: 2,
    max: 10
  }
}

console.log(configDB)

const db = require('knex')(configDB)

export = db
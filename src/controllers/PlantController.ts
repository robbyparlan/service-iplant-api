import { logger, db, sendResponseCustom, sendResponseError, 
  errorCodes, createError, validateParamsAll } from '../utils/util';
import 'dotenv/config';

class PlantController {
  /**
   * API Create JWT Token
   * @param {*} req 
   * @author Roby Parlan
   */
  async captureImage(req: any, res: any) {
    let trx
    try {
      let reqBody = req.body

      let rules = {
        nama_penyakit: 'required',
      }

      // Validate the request params
      await validateParamsAll(reqBody, rules)
        .catch((err) => {
          delete err.failed
          throw createError('', 'E_BAD_REQUEST', err)
          // return sendResponseCustom(res, err, 400)
        })

      let data = await db.select(db.raw(`*`))
        .from('m_medicine')
        .whereRaw(`disease = ?`, [reqBody.nama_penyakit])

      if (data.length === 0) throw createError(`Nama Penyakit ${reqBody.nama_penyakit} tidak ditemukan`, 'E_BAD_REQUEST')

      trx = await db.transaction()

      await trx('t_histories').insert({
        disease: data[0].disease,
        cure: data[0].cure,
      })

      await trx.commit()

      let result = {
        nama_penyakit: data[0].disease,
        obat: data[0].cure,
      }
      return sendResponseCustom(res, result)

    } catch (error: any) {
      if(trx) await trx.rollback()
      if (!errorCodes[error.code])
        logger.error(error)

      return sendResponseError(res, error)
    }
  }

}

export default PlantController